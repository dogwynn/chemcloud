# README #

```
sudo mkfs.ext4 -F /dev/disk/by-id/scsi-0DO_Volume_volume-nyc1-01
```

# Architecture

- Independent clients on student/researcher terminals
- Client sends input files to API server
- API server spins up CPU nodes to handle inputs
- CPU sends output to API server
- API server stores and caches output
- Client checks in periodically for updates
    - Error messages relayed when available
    - Ouput relayed when available


# API Components

- I/O: handles client communication 
    - Receives input files
    - Saves file to Inbox
    - Receives requests for status
    - Receives requests for output files
    - Returns files from Outbox
- Broker: 
    - Observes Inbox
    - Non-empty Inbox &rarr; request resources from Builder
- Builder:
    -

```
       Output   +------+                              +-------+
    +----------->      +--> Outbox <--+     +---------+       |
    |  Status   |      |              |     |         | CPU_N |
+---+----+      |      |           +--+-----v-+       | ...   |
| Client |      | I/O  |           |  Broker  |       | CPU_3 |
+---+----+      |      |           +--+--+--+-+       | CPU_2 |
    |           |      |              |  |  |         | CPU_1 |
    +----------->      +--> Inbox <---+  |  +--------->       |
       Input    +------+                 |            +---^---+
                                         |                |
                                         |          +-----+-----+
                                         +---------->  Builder  |
                                                    +-----------+

```

## DO sizes

Standard	s-1vcpu-1gb	1	1 GB	25 GB	1 TB	$5
Standard	s-1vcpu-2gb	1	2 GB	50 GB	2 TB	$10
Standard	s-1vcpu-3gb	1	3 GB	60 GB	3 TB	$15
Standard	s-2vcpu-2gb	2	2 GB	60 GB	3 TB	$15
Standard	s-3vcpu-1gb	3	1 GB	60 GB	3 TB	$15
Standard	s-2vcpu-4gb	2	4 GB	80 GB	4 TB	$20
Standard	s-4vcpu-8gb	4	8 GB	160 GB	5 TB	$40
Standard	s-6vcpu-16gb	6	16 GB	320 GB	6 TB	$80
Standard	s-8vcpu-32gb	8	32 GB	640 GB	7 TB	$160
Standard	s-12vcpu-48gb	12	48 GB	960 GB	8 TB	$240
Standard	s-16vcpu-64gb	16	64 GB	1,280 GB	9 TB	$320
Standard	s-20vcpu-96gb	20	96 GB	1,920 GB	10 TB	$480
Standard	s-24vcpu-128gb	24	128 GB	2,560 GB	11 TB	$640
Standard	s-32vcpu-192gb	32	192 GB	3,840 GB	12 TB	$960
