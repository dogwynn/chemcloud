from flask import Flask
from celery import Celery
from flask_restful import Api

from chemcloud_api.blueprints.api import create_api

CELERY_TASK_LIST = [
    'chemcloud_api.blueprints.cpu_builder.tasks',
]

def create_celery_app(app=None):
    """Create a new Celery object and tie together the Celery config to
    the app's config. Wrap all tasks in the context of the
    application.

    Args:
      app: Flask app

    Returns: Celery app

    """
    app = app or configured_app()

    celery = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'],
                    include=CELERY_TASK_LIST)
    celery.conf.update(app.config)
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery

def configured_app(settings_override:dict=None):
    '''Create a Flask application using the app factory pattern

    Args:
      settings_override (dict): Override settings

    Returns: Flask app
    '''
    app = Flask(__name__, instance_relative_config=True)

    app.config.from_object('config.settings')
    app.config.from_pyfile('settings.py', silent=True)

    if settings_override:
        app.config.update(settings_override)

    return app

def create_app(settings_override:dict=None):
    '''Create a Flask application using the app factory pattern

    Args:
      settings_override (dict): Override settings

    Returns: Flask app
    '''
    app = configured_app(settings_override)

    # Create REST API
    create_api(app)

    extensions(app)

    app.logger.info(app.url_map)
    # for rule in app.url_map.iter_rules():
    #     app.logger.info(rule)

    return app

def extensions(app):
    '''Register 0 or more extensions (mutates the app passed in).

    Args:
      app: Flask application instance

    Returns: None

    '''
    # api.init_app(app)
