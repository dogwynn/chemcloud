import time
import hashlib
import tempfile

import redis

from celery.utils.log import get_task_logger

from ...app import create_celery_app

celery = create_celery_app()
logger = get_task_logger(__name__)

def hash_content(content:str):
    return hashlib.sha256(content.encode()).hexdigest()

@celery.task()
def process_input(content:str):
    digest = hash_content(content)
    # Check for existence in inbox
    #with tempfile.TemporaryDirectory() as temp_dir:
        
    logger.info(digest)
    print('DIGEST:',digest)
    time.sleep(10)
    print('done')

@celery.task()
def build_server(*a):
    pass

