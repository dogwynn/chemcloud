from .generics import create_api_version
from . import api_v1
from . import api_v2

API_LIST = [
    api_v1,
    api_v2,
]

def create_api(app):
    for api_mod in API_LIST:
        create_api_version(
            api_mod.prefix, api_mod.register_resources,
            api_mod.api_bp,
            app
        )
