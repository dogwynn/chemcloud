import re
import io
from functools import partial, wraps

import werkzeug
from flask import Blueprint, current_app
from flask_restful import Resource, reqparse, abort

from .generics import api_prefix, tasks


TOKEN = 'f1cdbe7060e162cbcb56b2ad866944a7f4f93e8a341a756796255f43cddf2e04'
token_re = re.compile(r'Bearer (\w+)')

def auth_token(s):
    '''Given an Authorization header, parse out the token

    Args:
      s (str): Content of Authorization header

    Returns: (str) token

    '''
    match = token_re.match(s)
    if match:
        return match.groups()[0]
    return s


parser = reqparse.RequestParser()
parser.add_argument('Authorization', type=auth_token,
                    dest='auth_token',
                    location='headers')
parser.add_argument(
    'input', type=werkzeug.datastructures.FileStorage, location='files',
)

def valid_authorization(function):
    '''Decorator for validating authorization token

    If 'auth_token' not equal to (currently static) token, abort with
    403.

    '''
    @wraps(function)
    def wrapper(*a, **kw):
        args = parser.parse_args()
        current_app.logger.info(args['auth_token'])
        if not args['auth_token'] == TOKEN:
            abort(403, message='You do not have permission')
        return function(*a, **kw)
    return wrapper

def extract_input_file(args):
    '''Extract input file data

    Args:
      args (reqparse.Namespace): Parsed arguments namespace

    Returns: (bool, bytes) Whether input argument was provided and
      bytes content if it was
    '''
    input_data = args['input']
    if input_data is None:
        return False, None

    cio = io.BytesIO()
    input_data.save(cio)
    content = cio.getvalue()
    return True, content
    
def valid_input_file(args):
    '''Content from input file

    - If content not provided, abort with 400
    - If content is empty, abort with 400

    Args:
      args (reqparse.Namespace): Parsed arguments namespace

    Returns: (bytes) Content of input file

    '''
    provided, content = extract_input_file(args)
    if not provided:
        abort(400, message='You must provide input file data: "input"')
    elif not content:
        abort(400, message='Your input file data is empty')
    return content

class Input(Resource):
    @valid_authorization
    def get(self):
        return {}

    @valid_authorization
    def post(self):
        args = parser.parse_args()
        content = valid_input_file(args)
        current_app.logger.info(content)

        current_app.logger.info(tasks().process_input.delay(content.decode()))
        return {}
        
# ----------------------------------------------------------------------
# API interface
# --------------
#
# Need the following module attributes to be added to the list of
# APIs for the system:
#
# - prefix: prefixing function for this version
# - register_resources: function to register all resources associated
#      with this version
# - api_bp: Blueprint for this version
#

V1 = 'v1'
prefix = partial(api_prefix, V1)

def register_resources(api):
    api.add_resource(Input, '/input')

api_bp = Blueprint('api_bp_{}'.format(V1), __name__)
