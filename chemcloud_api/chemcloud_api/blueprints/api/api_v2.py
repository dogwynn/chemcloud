from functools import partial

from flask import Blueprint

from .generics import api_prefix


# ----------------------------------------------------------------------
# API interface
# --------------
#
# Need the following module attributes to be added to the list of
# APIs for the system:
#
# - prefix: prefixing function for this version
# - register_resources: function to register all resources associated
#      with this version
# - api_bp: Blueprint for this version
#

V2 = 'v2'
prefix = partial(api_prefix, V2)

def register_resources(api):
    pass

api_bp = Blueprint('api_bp_{}'.format(V2), __name__)
