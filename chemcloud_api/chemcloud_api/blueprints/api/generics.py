from flask_restful import Api

def api_prefix(v):
    return '/api/{}'.format(v)

def create_api_version(version_prefix, resource_register, api_bp, app):
    api = Api(api_bp, prefix=version_prefix())

    resource_register(api)

    app.register_blueprint(api_bp)
    return api

def tasks():
    class TaskNamespace:
        from ..cpu_builder.tasks import process_input
    return TaskNamespace
