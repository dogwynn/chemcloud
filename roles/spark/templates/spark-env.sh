source {{ hadoop_home }}/hadoop-client-env.sh
export SPARK_HOME={{ spark_home }}
export PATH=${PATH}:${SPARK_HOME}/bin
