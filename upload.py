import os
from pathlib import Path
from functools import partial

import slumber
import requests


def all_files_of_ext(root, ext):
    for root, dirs, files in os.walk(Path(root).expanduser().resolve()):
        for name in files:
            path = Path(root,name)
            if path.suffix == ext:
                yield path


all_docs = partial(all_files_of_ext, '~/Documents')
all_psi4 = partial(all_docs, '.psi4')


def chemcloud_api():
    return slumber.API('https://api.chemcloud.dataworkshed.net/api/v1')
    
